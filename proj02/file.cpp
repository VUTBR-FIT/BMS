/**
 * BMS 2. project
 * @author Lukáš Černý (xcerny63)
 */
#include "file.h"
#include <iostream>
#include <fstream>


bool readFile(std::vector< std::vector<char> > &content, std::string path, int lengthLimit, bool addLast) {
    std::ifstream input(path, std::ios::in | std::ios::binary);
    if (input.is_open()) {
        char c;
        std::vector<char> part;
        int partLength = 0;
        //! Read input file
        while(input.get(c)) {
            part.push_back(c);
            partLength++;
            if (partLength == lengthLimit) {
                partLength = 0;
                content.push_back(part);
                part.clear();
            }
        }
        if (partLength == 0 && addLast) {
            for (int i = 0; i < lengthLimit; i++) {
                part.push_back(0);
            }
        } else if (addLast) {
            char size = part.size();
            for (int i = size; i < lengthLimit; i++) {
                part.push_back(0);
            }
            part.back() = size;
        }
        if (addLast || partLength != 0)
            content.push_back(part);
    } else {
        std::cerr << "[ERROR] - Input file is not opened" << std::endl;
        return false;
    }
    input.close();
    return true;
}
bool writeFile(std::vector< std::vector<char> > content, std::string path) {
    std::ofstream output(path, std::ios::out | std::ios::binary | std::ios::trunc);
    if (output.is_open()) {
        std::vector<char> row;
        for (unsigned int i = 0; i < content.size(); i++) {
           row = content.at(i);
           output.write(&row[0], row.size() * sizeof(char));
        }
    } else {
        std::cerr << "[ERROR] - Output file is not opened" << std::endl;
        return false;
    }
    output.close();
    return true;
}

unsigned int getFileSize(std::string path) {
    std::ifstream input(path, std::ifstream::ate | std::ifstream::binary);
    if (input.is_open()) {
        std::streampos size = input.tellg();
        input.close();
        return size;
    } else {
        return 0;
    }
}
