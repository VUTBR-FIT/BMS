/**
 * BMS 2. project - bms2A - encoder
 * @author Lukáš Černý (xcerny63)
 */
#include <cstdlib>
#include <iostream>
#include <sstream>

#include "rs.h"
#include "file.h"

#ifndef NROOTS
#define NROOTS 3
#endif

/**
 * Make interliving & encode source data
 * @param[in] content Data to encoding
 * @param[out] encodedData Encoded data
 */
void encodeData(std::vector< std::vector<char> > content, std::vector< std::vector<char> > &encodedData);

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cerr << "[ERROR] - Missing input file" << std::endl;
    }

    std::vector< std::vector<char> > content; //! Loaded content
    std::vector< std::vector<char> > outputData; //! Prepared content to encode

    std::stringstream fileName;
    fileName << argv[1];

    if (!readFile(content, fileName.str(), (int) INPUT_LENGTH)) {
        return EXIT_FAILURE;
    }

    encodeData(content, outputData);

    fileName << ".out";
    if (!writeFile(outputData, fileName.str())) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

void encodeData(std::vector< std::vector<char> > content, std::vector< std::vector<char> > &encodedData) {
    ezpwd::RS<CODE_LENGTH, INPUT_LENGTH> encoder;

    std::vector<char> temp;
    //! Encode input data
    std::vector< std::vector<char> > matrix;
    for (unsigned int i = 0; i < content.size(); i++) {
        temp = content.at(i);
        encoder.encode(temp);
        matrix.push_back(temp);
    }

    //! Swap rows & columns
    encodedData.reserve(content.size());
    std::vector<char> newRow;
    for (unsigned int col = 0; col < CODE_LENGTH; col++) {
        newRow.clear();
        for (unsigned int row = 0; row < matrix.size(); row++) {
            temp = matrix.at(row);
            newRow.push_back(temp.at(col));
        }
        encodedData.push_back(newRow);
    }
}