/**
 * BMS 2. project - bms2B - decoder
 * @author Lukáš Černý (xcerny63)
 */
#include <cstdlib>
#include <iostream>
#include <sstream>

#include "rs.h"
#include "file.h"

#ifndef NROOTS
#define NROOTS 3
#endif

/**
 * Make interliving & input decode data
 *
 * @param[in] content Source data
 * @param[out] decodedData
 */
void decodeData(std::vector< std::vector<char> > content, std::vector< std::vector<char> > &decodedData);

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cerr << "[ERROR] - Missing input file" << std::endl;
    }

    std::vector< std::vector<char> > content; //! Loaded content
    std::vector< std::vector<char> > outputData; //! Decoded data to write

    std::stringstream fileName;
    fileName << argv[1];

    unsigned int size = getFileSize(fileName.str());

    if (!readFile(content, fileName.str(), (int) size / CODE_LENGTH, false)) {
        return EXIT_FAILURE;
    }

    decodeData(content, outputData);

    fileName << ".ok";
    if (!writeFile(outputData, fileName.str())) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

void decodeData(std::vector< std::vector<char> > content, std::vector< std::vector<char> > &decodedData) {
    ezpwd::RS<CODE_LENGTH, INPUT_LENGTH> decoder;

    //! Deinterliving input data
    std::vector< std::vector<char> > matrix;
    std::vector<char> newRow, temp;
    newRow.reserve(CODE_LENGTH);
    //! Swap rows and columns
    for (unsigned int col = 0; content.size() > 0 && col <  content.front().size(); col++) {
        newRow.clear();
        for (unsigned int row = 0; row < content.size(); row++) {
            temp = content.at(row);
            newRow.push_back(temp.at(col));
        }
        matrix.push_back(newRow);
    }

    newRow.clear();
    decodedData.reserve(content.size());
    //! Decode vectors
    for (unsigned int row = 0; row < matrix.size(); row++) {
        temp = matrix.at(row);
        decoder.decode(temp);
        if (row == matrix.size() - 1) {//! Last vector has size
            unsigned int size = temp.at(INPUT_LENGTH - 1);
            temp.erase(temp.begin() + size, temp.end());
        } else { //! Remove empty char from the end
            temp.erase(temp.begin() + INPUT_LENGTH, temp.end());
        }
        if (!temp.empty()) //! Last vector could be empty
            decodedData.push_back(temp);
    }
}