/**
 * BMS 2. project
 * This library use Ezpwd Reed-Solomon -- Reed-Solomon encoder / decoder library (https://github.com/pjkundert/ezpwd-reed-solomon)
 * @author Lukáš Černý (xcerny63)
 */
#include <vector>
#include <string>

#ifndef BMS_FILE_H
#define BMS_FILE_H

#ifndef CODE_LENGTH
#define CODE_LENGTH 255 // THIS IS 175% OF SOURCE FILE
#endif

#ifndef CODE_LENGTH_PREC
#define CODE_LENGTH_PREC 175
#endif

#ifndef INPUT_LENGTH_PREC
#define INPUT_LENGTH_PREC 100
#endif

#ifndef INPUT_LENGTH
#define INPUT_LENGTH CODE_LENGTH * INPUT_LENGTH_PREC / CODE_LENGTH_PREC + 1 // OUTPUT SIZE MUST BE <= 175% SOURCE FILE
#endif

/**
 * Read input file
 * @param[out] content Loaded data saved as vectors
 * @param[in] path Input file
 * @param[in] lengthLimit Length of subvector
 * @param[in] addLast Flag for add adding last vector
 * @return Success of operation
 */
bool readFile(std::vector< std::vector<char> > &content, std::string path, int lengthLimit, bool addLast = true);
/**
 * Write array of vector to the output file
 * @param[in] content Content to write
 * @param[in] path Output file
 * @return Success of operation
 */
bool writeFile(std::vector< std::vector<char> > content, std::string path);
/**
 * Get size of file
 * @param[in] path Path to source file
 * @return Length of file
 */
unsigned int getFileSize(std::string path);

#endif //BMS_FILE_H
