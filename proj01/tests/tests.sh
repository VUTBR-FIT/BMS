#!/bin/bash

TASK_A=./bms1A
TASK_B=./bms1B

ok_count=0
count=0

run () {
    count=$((count+1))

    printf "RUN %s %s\n" $TASK_A "$1 $2"
    $TASK_A "$1$2"

    printf "RUN %s %s.wav\n" $TASK_B "$1"
    $TASK_B "$1.wav"



    retCode=$(diff "$1.txt" "$1$2_ref")
    if [ "$retCode" = "0" ] || [ "$retCode" = "" ]; then
        ok_count=$((ok_count+1))
    fi
}

run "./tests/example_00" ""
run "./tests/example_01" ".txt"
run "./tests/example_02" ".txt"

printf "Stats SUM: %s; SUCCESS: %s\n" "$count" "$ok_count"

if [ $ok_count -eq $count ]; then
    exit 0
else
    exit 1
fi