/**
 * File: bms1A.cpp
 *
 * @author Lukáš Černý (xcerny63)
 */

#include <iostream>
#include <cstdlib>
#include <math.h>

#include "Constants.h"
#include "TxtFile.h"
#include "WavFile.h"

using namespace std;

int main(int argc, char** argv) {

    if (argc != 2) {
        cerr << "[ERROR]\t - Program requires only one parameter" << endl;
        return EXIT_FAILURE;
    }

    TxtFile *input = new TxtFile(argv[1]);
    if (!input->read()) {
        cerr << "[ERROR]\t - Reading input file -> FAILED" << endl;
        return EXIT_FAILURE;
    }

    input->setPrefix("00110011");
    WavFile *output = new WavFile(File::changeExtension(argv[1], "wav"));
    //! Configure output
    output->setContent(input->getContent(true));
    output->sampleRate = SAMPLE_RATE;
    output->channels = CHANNELS;
    output->format = FORMAT;
    output->amplitude = AMPLITUDE;
    output->countAmplitudes = COUNT_AMPLITUDES;
    output->frequency = 1000.0;

    if (!output->write()) {
        cerr << "[ERROR]\t - Writing the output file -> FAILED" << endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}


