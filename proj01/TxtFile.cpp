/**
 * File: TxtFile.cpp
 *
 * @author Lukáš Černý (xcerny63)
 */

#include "TxtFile.h"

bool TxtFile::read() {
    ifstream input;
    input.open(getPath());
    if (input.is_open()) {
        string buffer, content;
        //! Add prefix of content
        content = this->getPrefix();
        //! Read the input file
        while (getline(input, buffer)) {
            content += buffer;
        }
        //! Set the content
        this->setContent(content);
    } else {
        return false;
    }
    return true;
}

bool TxtFile::write() {
    ofstream output;
    //! Open file
    output.open(this->getPath());
    //! Write content
    output << this->getContent();
    //! Close file
    output.close();
    return true;
}