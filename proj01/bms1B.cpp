/**
 * File: bms1B.cpp
 *
 * @author Lukáš Černý (xcerny63)
 */

#include <cstdlib>
#include <math.h>

#include "Constants.h"
#include "TxtFile.h"
#include "WavFile.h"

#include "sndfile.hh"

using namespace std;

int main(int argc, char** argv) {
    if (argc != 2) {
        cerr << "[ERROR]\t - Program requires only one parameter" << endl;
        return EXIT_FAILURE;
    }

    WavFile *input = new WavFile(argv[1]);
    //! Configure input data
    input->amplitude = AMPLITUDE;
    input->countAmplitudes = COUNT_AMPLITUDES;
    input->setPrefix("00110011");

    if (!input->read()) {
        cerr << "[ERROR]\t - Reading input file -> FAILED" << endl;
        return EXIT_FAILURE;
    }

    TxtFile *output = new TxtFile(File::changeExtension(argv[1], "txt"));
    output->setContent(input->getContent(false));

    if (!output->write()) {
        cerr << "[ERROR]\t - Writing the output file -> FAILED" << endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

