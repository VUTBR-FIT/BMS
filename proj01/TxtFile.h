/**
 * File: TxtFile.h
 *
 * @author Lukáš Černý (xcerny63)
 */

#ifndef BMS_TXTFILE_H
#define BMS_TXTFILE_H

#include "File.h"

class TxtFile : public File {
public:
    TxtFile(string path) : File(path) {}
    bool read();
    bool write();
};

#endif //BMS_TXTFILE_H
