/**
 * File: File.h
 *
 * @author Lukáš Černý (xcerny63)
 */

#include <iostream>
#include <fstream>

#ifndef BMS_FILE_H
#define BMS_FILE_H

using namespace std;

/**
 * Class File
 *
 * @author Lukáš Černý (xcerny63)
 *
 * Base functions for work with file
 */
class File {
public:
    /** delimiter in path */
    char delimiter = '/';

    /**
     * Set path of output/input file
     * @param path
     */
    File(string path) {
        this->path = path;
    }

    /**
     * Read input file and content convert to string
     *
     * @return Success of operation
     */
    virtual bool read() = 0;

    /**
     * Convert content and write it to the output file
     *
     * @return Success of operation
     */
    virtual bool write() = 0;

    /**
     * Return content of the file with prefix or without prefix
     *
     * @param prefix Add prefix
     * @return Content of file
     */
    string getContent(bool prefix = false) {
        if (prefix) {
            return this->prefix + this->content;
        } else {
            return content;
        }
    }

    /**
     * Set content of the output file
     *
     * @param content
     */
    void setContent(string content) {
        this->content = content;
    }

    /**
     * @return Prefix of content
     */
    string getPrefix() {
        return prefix;
    }

    /**
     * Set prefix of the content
     *
     * @param prefix
     */
    void setPrefix(string prefix) {
        this->prefix = prefix;
    }

    /**
     * @return Path to the file
     */
    string getPath() {
        return path;
    }

    /**
     * Check if file exists
     *
     * @param path Path to the file
     * @return True if exists otherwise false
     */
    static bool fileExists(string path) {
        ifstream stream(path.c_str());
        return stream.good();
    }

    /**
     * Swap current file extension with new file extension
     * @param path Path to the file
     * @param extension New file extension
     * @return Path to the new file
     */
    static string changeExtension(string path, string extension) {
        unsigned int index = path.find_last_of('.');
        string newPath = "";
        if (path.length() < index || index == 0 || index == string::npos) {
            newPath = path + ".";
        } else {
            newPath = path.substr(0, index + 1);
        }
        return newPath + extension;
    }
private:
    /** Path to the file */
    string path;
    /** Content of the file */
    string content;
    /** Prefix of the content */
    string prefix;
};

#endif //BMS_FILE_H
