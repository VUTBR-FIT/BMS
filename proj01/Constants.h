/**
 * File: Constants.h
 *
 * @author Lukáš Černý (xcerny63)
 */

#ifndef BMS_CONSTANTS_H
#define BMS_CONSTANTS_H

#define SAMPLE_RATE 18000
#define CHANNELS 1
#define FORMAT (SF_FORMAT_WAV | SF_FORMAT_PCM_24)
#define AMPLITUDE (1.0 * 0x7F000000)
#define COUNT_AMPLITUDES 4
#define FREQ 1000.0

#endif //BMS_CONSTANTS_H
