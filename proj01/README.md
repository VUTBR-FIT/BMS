# BMS - project 01  

## Author - Lukáš Černý (xcerny63)  

## CLI:  
 - compile  
`make all` 
 - run modulator  
`./bms1A file_name.txt`
- run demodulator
`./bms1B file_name.wav`
