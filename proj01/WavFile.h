/**
 * File: WavFile.h
 *
 * @author Lukáš Černý (xcerny63)
 */

#ifndef BMS_WAVFILE_H
#define BMS_WAVFILE_H

#include <vector>

#include "File.h"
#include "sndfile.hh"

using namespace std;

/**
 * Class WavFile
 *
 * @author Lukáš Černý (xcerny63)
 */
class WavFile : public File {
public:
    /** Sample rate */
    int sampleRate = 1000.0;
    /** Used channels */
    int channels = 2;
    /** Output format */
    int format = SF_FORMAT_WAV;
    /** Number of amplitudes */
    int countAmplitudes = 1;
    /** */
    double amplitude = 1000.0;
    /** Frequency of the input file */
    double frequency = 1000.0;
    WavFile(string path) : File(path) {}
    bool read();
    bool write();
private:
    /** Read input as absolute */
    bool absolute = false;
    /** Generated amplitudes */
    vector<int> amplitudes;

    /**
     * @return Count of samples
     */
    int getCountSamples() {
        return (int) sampleRate / frequency;
    }

    /**
     * Generate amplitudes
     */
    void generateAmplitudes();

    /**
     * Read part of content and return corresponding amplitude
     *
     * @return Corresponding amplitude
     */
    int getAmplitude();

    /**
     * @return Generated sampled sinus
     */
    vector<double> generateSampledSinus();

    /**
     * Read synchronize bits and set sample period
     *
     * @param content
     * @return Success of operation
     */
    bool getSampledPeriod(vector<int> content);

    /**
     *
     *
     * @param content
     * @param startIndex
     * @param sinus
     * @return
     */
    string decodeAmplitude(vector<int> content, int startIndex, vector<double> sinus);
};

#endif //BMS_WAVFILE_H
