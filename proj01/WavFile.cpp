/**
 * File: WavFile.cpp
 *
 * @author Lukáš Černý (xcerny63)
 */

#include <climits>
#include <math.h>
#include <vector>
#include <iostream>
#include "WavFile.h"

using namespace std;

bool WavFile::read() {
    vector<int> content;
    this->absolute = true;
    SndfileHandle input = SndfileHandle(this->getPath().c_str());
    this->sampleRate = input.samplerate();
    //! Input buffer
    int *buffer = new int[this->sampleRate];
    int64_t readSamples;
    while((readSamples = input.read(buffer, this->sampleRate))) {
        for (int i = 0; i < readSamples; i++) {
            content.push_back(abs(buffer[i]));
        }
    }
    delete [] buffer;

    if (!this->getSampledPeriod(content)) {
        cerr << "[ERROR]\t - Please set synchronization prefix before reading input file" << endl;
        return false;
    }

    //! Generate amplitudes & sampled sinus
    this->generateAmplitudes();
    vector<double> sinus = this->generateSampledSinus();

    //! Convert content of the input file to string 0,1
    for (unsigned int i = this->getCountSamples() * this->getPrefix().length() / 2; i < content.size(); i += this->getCountSamples()) {
        this->setContent(this->getContent() + this->decodeAmplitude(content, i, sinus));
    }
    return true;
}
bool WavFile::write() {
    //! Output buffer
    vector<int> buffer;
    //! Generate amplitudes & sampled sinus
    vector<double> sinus = this->generateSampledSinus();
    this->generateAmplitudes();

    int sinIndex = 0;
    //! Convert input string to output format WAW
    while (!this->getContent().empty()) {
        //! Get amplitude by part of input string
        int amplitude = this->getAmplitude();
        for (int index = 0; index < this->getCountSamples(); index++) {
            //! Append sample of the sinus to the output buffer
            buffer.push_back(sinus[sinIndex++ % this->sampleRate] * amplitude);
        }
    }

    //! Open the output stream & write it to the file
    SndfileHandle output = SndfileHandle(this->getPath().c_str(), SFM_WRITE, format, channels, sampleRate);
    output.write(&(buffer[0]), buffer.size());
    return true;
}
void WavFile::generateAmplitudes() {
    amplitudes.clear();
    for (int i = 0; i < countAmplitudes; i++) {
        //! Convert basic amplitude to smaller amplitude
        amplitudes.push_back((this->amplitude /(double)(countAmplitudes - 1)) * i);
    }
}
vector<double> WavFile::generateSampledSinus() {
    vector<double> sampledSinus;
    double temp = this->frequency / this->sampleRate;
    double tempSin;
    for (int i = 0; i < this->sampleRate; i++) {
        //! Get value (= sample) of sinus
        tempSin = sin(2 * M_PI * temp * i);
        //! If required then convert to absolute value
        if (this->absolute && tempSin < 0.0) {
            tempSin = -tempSin;
        }
        //! Insert sample
        sampledSinus.push_back(tempSin);
    }
    return sampledSinus;
}
int WavFile::getAmplitude() {
    string content = this->getContent();
    string part = "";

    //! Fix: Content has odd length
    if (content.length() < 2) {
        part = content.substr(0, 1);
        setContent(content.substr(1, content.length()));
    } else {
        part = content.substr(0, 2);
        setContent(content.substr(2, content.length()));
    }

    //! String to the corresponding amplitude
    if (part.compare("00") == 0)
        return amplitudes[0];
    else if (part.compare("01") == 0)
        return amplitudes[1];
    else if (part.compare("10") == 0)
        return amplitudes[2];
    else
        return amplitudes[3];
}
bool WavFile::getSampledPeriod(vector<int> content) {
    //! Get count of cycles
    unsigned int syncPrefix = this->getPrefix().length() / 2;
    //! If synchronize prefix missing or prefix have odd length
    if (syncPrefix == 0 || this->getPrefix().length() % 2 != 0) {
        return false;
    }

    unsigned int cycles = 0,
        sample = 0;
    bool isOut = false;
    //! Read prefix
    for (sample = 0; sample < content.size() - 1; sample++) {
        if (isOut == true) {
            if (content[sample] == 0 && content[sample + 1] == 0) {
                cycles++;
                break;
            }
        } else {
            if (content[sample]) {
                isOut = true;
                cycles++;
            }
        }
        if (cycles == syncPrefix) {
            break;
        }
    }

    this->frequency = (int) (this->sampleRate / (sample / cycles));
    return true;
}
string WavFile::decodeAmplitude(vector<int> content, int startIndex, vector<double> sinus) {
    int currentAmplitude = 0;
    //! Get amplitude of input string
    for (int i = startIndex; i < startIndex + this->getCountSamples(); i++) {
        if (currentAmplitude < content[i])
            currentAmplitude = content[i];
    }

    int difference = INT_MAX;
    int min = 0;
    //! Compare amplitude of input with generated amplitudes
    for (int i = 0; i < this->countAmplitudes; i++) {
        if (difference > abs(this->amplitudes[i] - currentAmplitude)) {
            min = i;
            difference = abs(this->amplitudes[i] - currentAmplitude);
        }
    }

    switch (min) {
        case 0:
            return "00";
        case 1:
            return "01";
        case 2:
            return "10";
        default:
            return "11";
    }
}
